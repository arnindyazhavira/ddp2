public class PegawaiSpesial extends Pegawai implements bisaLibur{
    private String levelKeahlian;

    public PegawaiSpesial(String nama, int uang, String levelKeahlian){
        super(nama, uang, levelKeahlian);
    }

    public String bekerja(){
        return(getNama() + " bekerja di kedai VoidMain");
    }

    public void setLevelKeahlian(String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
    }

    public String getLevelKeahlian(){
        return levelKeahlian;
    }

    @Override
    public String bicara(){
        return("Halo, saya" + getNama() + ". Uang saya adalah " + getUang() + ", dan level keahlian saya adalah" + getLevelKeahlian() + ". Saya memiliki privilege yaitu bisa libur.");
    }

    @Override
    public String libur(){
        return(getNama() + " sedang berlibur ke Akihabara");
    }

    public String toString(){
        return bicara();
    }
}
    

