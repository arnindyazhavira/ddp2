import java.util.HashMap;
import java.util.Scanner;


public class Main {
    public static void main(String[] args){
        HashMap<String, Makhluk> makhluk = new HashMap<String, Makhluk>();
        Manusia orang1 = new Pegawai("yoga", 100000, "master");
        Manusia orang2 = new Pelanggan("bujang", 100000);
        Manusia orang3 = new PegawaiSpesial("arnin", 100000, "master");
        Hewan hewan1 = new Ikan("betta", "Betta Splendens",false);
        Hewan hewan2 = new BurungHantu("burhan", "Bubo Scandiacus");
        Hewan hewan3 = new IkanSpesial("deija", "Betta Splendens",false);
        Scanner input = new Scanner(System.in);

        // if(identitas.equals("pegawai")){
            // Pegawai pegawai = new Pegawai(data[0], uang, umur, data[3]);
        
        //else if(identitas.equals("pelanggan")){
           //  Pelanggan02 pelanggan = new Pelanggan02(data[0], uang, umur);
        makhluk.put("yoga", orang1);
        makhluk.put("bujang", orang2);
        makhluk.put("brnin", orang3);
        makhluk.put("betta", hewan1);
        makhluk.put("burhan", hewan2);
        makhluk.put("deija", hewan3);
        

    
        while (true) {
            Scanner inputPerintah = new Scanner(System.in);
            System.out.println("Silakan masukkan perintah: ");
            String[] perintah = new String[2];
            perintah = inputPerintah.nextLine().toLowerCase().split(" ");

            if(perintah[0].equals("selesai")){
                System.out.println("Sampai jumpa!");
            }else if((perintah[0]).equals("panggil")){
                if(makhluk.containsKey(perintah[1])){
                    System.out.println(makhluk.get(perintah[1]).toString());
                }else{
                    System.out.println("Maaf" + perintah[1]+ "tidak pernah melewati/mampir di kedai");
                }
            }

            else {
                if(makhluk.containsKey(perintah[0])){
                    switch(perintah[1]){
                        case "bicara" : 
                        if(makhluk.get(perintah[0]) instanceof Manusia){
                            System.out.println(((Manusia)makhluk.get(perintah[0])).bicara());break;
                        } else {
                            System.out.println("Perintah tidak dapat dilakukan");break;
                        }
                        case "beli" :
                        if(makhluk.get(perintah[0]) instanceof Pelanggan) {
                            System.out.println(((Pelanggan)makhluk.get(perintah[0])).beli());break;
                        } else {
                            System.out.println("Perintah tidak dapat dilakukan");
                        }; break;
                        case "bersuara" : 
                        if(makhluk.get(perintah[0]) instanceof Hewan){
                            System.out.println(((Hewan)makhluk.get(perintah[0])).bersuara());break;
                        } else {
                            System.out.println("Perintah tidak dapat dilakukan");break;
                        }
                        case "bekerja" :
                        if(makhluk.get(perintah[0]) instanceof Pegawai){
                            System.out.println(((Pegawai)makhluk.get(perintah[0])).bekerja());break;
                        } else {
                            System.out.println("Perintah tidak dapat dilakukan");break;
                        }
                        case "bergerak" : System.out.println(makhluk.get(perintah[0]).bergerak());break;
                        case "bernafas" : System.out.println(makhluk.get(perintah[0]).bernafas());break;                       
                        case "terbang" : 
                        if(makhluk.get(perintah[0]) instanceof IkanSpesial){
                            System.out.println(((IkanSpesial)makhluk.get(perintah[0])).terbang());break;
                        } 
                        
                        case "libur" :
                        if(makhluk.get(perintah[0]) instanceof PegawaiSpesial){
                            System.out.println(((PegawaiSpesial)makhluk.get(perintah[0])).libur());break;
                        }
                        else{
                            System.out.println("Maaf, Pegawai " + perintah[0] + " tidak bisa libur.");
                        }
                        default : 
                        if (makhluk.get(perintah[0]) instanceof Pegawai){
                            System.out.printf("Maaf, pegawai %s tidak bisa %s. %n", ((Pegawai)makhluk.get(perintah[0])).getNama(), perintah[1]);
                        } else if (makhluk.get(perintah[0] instanceof Pelanggan)){
                            System.out.printf("Maaf, pelanggan %s tidak bisa %s. %n", ((Pelanggan)makhluk.get(perintah[0])).getNama(), perintah[1]);
                        } else if (makhluk.get(perintah[0] instanceof Ikan)){
                            System.out.printf("Maaf, ikan %s tidak bisa %s. %n", ((Ikan)makhluk.get(perintah[0])).getNama(), perintah[1]);
                        } else if (makhluk.get(perintah[0] instanceof BurungHantu)){
                            System.out.printf("Maaf, burung hantu %s tidak bisa %s. %n", ((BurungHantu)makhluk.get(perintah[0])).getNama(), perintah[1]);
                        }
                            
                    }
                }
                else {
                    System.out.printf("Tidak ada yang bernama %s%n", perintah[0]);
                }
            }
        }
    }
}

