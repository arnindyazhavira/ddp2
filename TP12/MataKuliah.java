import java.util.ArrayList;

public class MataKuliah {
    private String nama;
    private String kode;
    private ArrayList<Mahasiswa> daftarMhs = new ArrayList<>();
    private ArrayList<Manusia> manusia = new ArrayList<>();
    private ArrayList<Dosen> daftarDosen = new ArrayList<>();


    public MataKuliah(String nama, String kode) {
        this.nama = nama;
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public ArrayList<Mahasiswa> getDaftarMhs() {
        return daftarMhs;
    }

    public void setDaftarMhs(ArrayList<Mahasiswa> daftarMhs) {
        this.daftarMhs = daftarMhs;
    }

    public void setKode(String kode){
        this.kode = kode;
    }

    public void tambahMhs(ArrayList<Mahasiswa> mhs){
        for(Mahasiswa i: mhs){
            if (i == null){
                this.daftarMhs.add(i);
            }
        }
    }

    public void dropMhs(ArrayList<Mahasiswa> mhs){
        try{
            for(Mahasiswa i: mhs){
                if (i.equals(mhs)){
                    this.daftarMhs.remove(i);
                }
            }
            daftarMhs.remove(mhs);
        } catch (java.util.NoSuchElementException x){
            System.out.println("Keyboard Interrupt, Program has been terminated");
        }
        
    }

    public void assignDosen(ArrayList<Manusia> dosen){
        for(Manusia i: dosen){
            if (i == null){
                this.daftarDosen.add(i);
            }
        }
    }


}
