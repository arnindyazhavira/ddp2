import java.util.*;

public class TP1_3 {
    public static void main(String args[]) {
        System.out.println("Selamat datang di bank Dedepedua!");
        Scanner sc = new Scanner(System.in);
        System.out.print("Apa tipe kartu anda?: "); 
        String tipe = sc.nextLine().toLowerCase(); 
        System.out.print("Sejak tahun berapa anda menjadi member?: "); 
        int year = sc.nextInt(); 
        int hargaBarang = 200000;
        if (tipe.equals("gold") & year > 2017){
            System.out.println("Dapat diskon sebanyak 25 persen.");
            System.out.println("Harga diskon barang menjadi "+ (hargaBarang-50000));
        } else if (tipe.equals("gold") & year <= 2017){
            System.out.println("Dapat diskon sebanyak 30 persen");
            System.out.println("Harga diskon barang menjadi "+ (hargaBarang-60000));
        }
        if (tipe.equals("silver") & year > 2017){
            System.out.println("Dapat diskon sebanyak 15 persen.");
            System.out.println("Harga diskon barang menjadi "+ (hargaBarang-30000));
        } else if (tipe.equals("silver") & year <= 2017){
            System.out.println("Dapat diskon sebanyak 20 persen");
            System.out.println("Harga diskon barang menjadi "+ (hargaBarang-40000));
        }
        if (tipe.equals("bronze") & year > 2017){
            System.out.println("Dapat diskon sebanyak 5 persen.");
            System.out.println("Harga diskon barang menjadi "+ (hargaBarang-10000));
        } else if (tipe.equals("bronze") & year <= 2017){
            System.out.println("Dapat diskon sebanyak 10 persen");
            System.out.println("Harga diskon barang menjadi "+ (hargaBarang-20000));
        }
     }
}
                
            