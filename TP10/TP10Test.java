import org.junit.Test;
import static org.junit.Assert.*;
//import org.junit.jupiter.api.Test;

public class TP10Test {
    @Test
    public void testPelangganBicara(){
        Pelanggan bujang = new Pelanggan("bujang", 100000);
        assertEquals("Halo, saya bujang. Uang saya adalah 100000", bujang.bicara());
    }
     @Test
     public void testPelangganBeli(){
         Pelanggan bujang = new Pelanggan("bujang", 100000);
         assertEquals("bujang membeli makanan dan minuman di kedai VoidMain", bujang.beli());
     }
     @Test
     public void testPelangganBergerak(){
        Pelanggan bujang = new Pelanggan("bujang", 100000);
        assertEquals("bujang bergerak dengan cara berjalan", bujang.bergerak());
     }
     @Test
     public void testPelangganBernafas(){
        Pelanggan bujang = new Pelanggan("bujang", 100000);
        assertEquals("bujang bernafas dengan paru-paru", bujang.bernafas());
     }
     @Test
     public void testPegawaiBergerak(){
        Pegawai yoga = new Pegawai("yoga", 100000, "master");
        assertEquals("yoga bergerak dengan cara berjalan", yoga.bergerak());
     }
     @Test
     public void testPegawaiBicara(){
        Pegawai yoga = new Pegawai("yoga", 100000, "master");
        assertEquals("Halo, saya yoga. Uang saya adalah 100000, dan level keahlian saya adalah master.", yoga.bicara());
     }
     @Test
     public void testPegawaiBernafas(){
        Pegawai yoga = new Pegawai("yoga", 100000, "master");
        assertEquals("yoga bernafas dengan paru-paru", yoga.bernafas());
     }
     @Test
     public void testPegawaiBekerja(){
        Pegawai yoga = new Pegawai("yoga", 100000, "master");
        assertEquals("yoga bekerja di kedai VoidMain", yoga.bekerja());
     }
     @Test
     public void testPegawaiSpesial(){
         PegawaiSpesial arnin = new PegawaiSpesial("arnin", 2000, "pemula");
         assertEquals("arnin sedang berlibur ke Akihabara", arnin.libur());
     }
     @Test
     public void testIkanBersuara(){
         Ikan betta = new Ikan("betta", "Betta Splendens", true);
         assertEquals("Blub blub blub blub. Blub.(Halo, saya betta. Saya adalah ikan yang beracun).", betta.bersuara());
     }
    @Test
    public void testIkanBergerak(){
        Ikan betta = new Ikan("betta", "Betta Splendens", true);
        assertEquals("betta bergerak dengan cara berenang", betta.bergerak());
    }
    @Test
    public void testIkanBernafas(){
        Ikan betta = new Ikan("betta", "Betta Splendens", true);
        assertEquals("betta bernafas dengan insang", betta.bernafas());
    }
    @Test
    public void testBurungHantuBergerak(){
        BurungHantu burhan = new BurungHantu("burhan", "Burung Hantu");
        assertEquals("burhan bergerak dengan cara terbang", burhan.bergerak());
    }
    @Test
    public void testBurungHantuBernafas(){
        BurungHantu burhan = new BurungHantu("burhan", "Burung Hantu");
        assertEquals("burhan bernafas dengan paru-paru", burhan.bernafas());
    }
    @Test
    public void testBurungHantuBersuara(){
        BurungHantu burhan = new BurungHantu("burhan", "Burung Hantu");
        assertEquals("Hooooh hoooooooh.(Halo, saya burhan. Saya adalah burung hantu).", burhan.bersuara());
    }
    @Test
    public void testPelangganAtribut(){
        Pelanggan yoga = new Pelanggan("yoga", 2000);
        assertEquals("yoga", yoga.getNama());
        assertEquals(2000, yoga.getUang());
    }
    @Test
    public void testPegawaiAtribut(){
        Pegawai bujang = new Pegawai("bujang", 2000, "master");
        assertEquals("bujang", bujang.getNama());
        assertEquals(2000, bujang.getUang());
        assertEquals("master", bujang.getLevelKeahlian());
    }
    @Test
    public void testBurungHantuAtribut(){
        BurungHantu burhan = new BurungHantu("burhan", "Burung Hantu");
        assertEquals("burhan", burhan.getNama());
        assertEquals("Burung Hantu", burhan.getSpesies());
    }
    @Test
    public void testSubclassPelanggan(){
        assertTrue(Manusia.class.isAssignableFrom(Pelanggan.class));
    }
    @Test
    public void testSubclassPegawai(){
        assertTrue(Manusia.class.isAssignableFrom(Pegawai.class));
    }
    @Test
    public void testSubclassPegawaiSpesial(){
        assertTrue(Manusia.class.isAssignableFrom(PegawaiSpesial.class));
    }
    @Test
    public void testSubclassIkan(){
        assertTrue(Hewan.class.isAssignableFrom(Ikan.class));
    }
    @Test
    public void testSubclassBurungHantu(){
        assertTrue(Hewan.class.isAssignableFrom(BurungHantu.class));
    }

}

