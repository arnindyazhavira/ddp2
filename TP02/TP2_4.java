import java.util.Scanner;
import java.math.*;

public class TP2_4 {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        String pemimpin = "";
        int temp;
        if (a>0 && b>0 && c>0){
            if (((a < b) && (b < c)) || ((c < b) && (b < a))){
                pemimpin += "b memimpin mereka";
            } else if (((b<a) && (a<c)) || ((c < a) && (a<b))){
                pemimpin += "a memimpin mereka";
            } else if (((a < c) && (c < b)) || ((b < c) && (c < a))){
                pemimpin += "c memimpin mereka";
            }
        
            if (a > c) {
                temp = c;
                c = a;
                a = temp;
            } 
            if (b > c) {
                temp = c;
                c = b;
                b = temp;
            }
    
            if (Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c,2)){
                System.out.println("segitiga siku-siku, mereka penyusup");
                System.out.println(pemimpin);
            }     
            else{
                System.out.println("bukan segitiga siku-siku, mereka bukan penyusup");
            }
        }
        
    }
}