public class Pelanggan extends Manusia{         

    public Pelanggan(String nama, int uang){
        super(nama, uang);
    }

    public String beli(){
        return(getNama().toLowerCase() + " membeli makanan dan minuman di kedai VoidMain");
    }

    @Override
    public String bicara(){
        return("Halo, saya " + getNama().toLowerCase() + ". Uang saya adalah " + getUang());
    }

    public String toString(){
        return bicara();
    }
}
    

