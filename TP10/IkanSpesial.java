public class IkanSpesial extends Ikan implements bisaTerbang{
    private boolean beracun;

    public IkanSpesial(String nama, String spesies, boolean beracun){
        super(nama, spesies, beracun);
    }

    public boolean isBeracun(){
        return beracun;
    }

    @Override
    public String bernafas(){
        return(getNama() + "bernafas dengan insang");
    }

    @Override
    public String bersuara(){
        if (this.beracun == (false)) {
            return("Blub blub blub blub. Blub.(Halo, saya" + getNama() + "Saya adalah ikan yang tidak beracun. Saya bisa terbang loh.).");
        }
        else{
            return("Blub blub blub blub. Blub.(Halo, saya" + getNama() + "Saya adalah ikan yang beracun. Saya bisa terbang loh.).");
        }
        
    }

    @Override
    public String bergerak(){
        return(getNama() + "bergerak dengan cara berenang");
    }

    @Override
    public String terbang(){
        return("Fwooosssshhhhh! Plup.");
    }

    public String toString(){
        return bersuara();
    }
}
