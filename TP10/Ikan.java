public class Ikan extends Hewan {
    private boolean beracun;

    public Ikan(String nama, String spesies, boolean beracun){
        super(nama, spesies);
        this.beracun = beracun;
    }

    public boolean isBeracun(){
        return beracun;
    }

    @Override
    public String bernafas(){
        return(getNama() + " bernafas dengan insang");
    }

    @Override
    public String bersuara(){
        if (this.beracun == (false)) {
            return("Blub blub blub blub. Blub.(Halo, saya " + getNama() + ". Saya adalah ikan yang tidak beracun).");
        }
        else{
            return("Blub blub blub blub. Blub.(Halo, saya " + getNama() + ". Saya adalah ikan yang beracun).");
        }
        
    }

    @Override
    public String bergerak(){
        return(getNama() + " bergerak dengan cara berenang");
    }

    public String toString(){
        return bersuara();
    }
}
