import org.junit.Test;
import static org.junit.Assert.*;

public class MahasiswaTest {
    @Test
    public void kuliah1(){
        MataKuliah matkul1 = new MataKuliah("DDP2", "CSGE601021" );
        MataKuliah matkul2 = new MataKuliah("DDP1", "CSGE601020" );
        Mahasiswa mhs1 = new Mahasiswa("Burhan", "210283371");
        

        mhs1.tambahMatkul(matkul1);
        mhs1.tambahMatkul(matkul2);
        mhs1.dropMatkul(matkul1);

        ArrayList<MataKuliah> daftarMatkul = mhs1.getDaftarMatkul();
        assertEquals(mhs1.getNama(), "Burhan");
        assertEquals(mhs1.getKode(), "2102833571");

    }
}
