public abstract class Manusia implements Makhluk{
    private int uang;
    private String nama;

    public Manusia(){}

    public Manusia(String nama, int uang){
        this.nama = nama;
        this.uang = uang;
    }

    public abstract String bicara();

    @Override
    public String bernafas(){
        return(getNama() + " bernafas dengan paru-paru");
    }

    @Override
    public String bergerak(){
        return(getNama() + " bergerak dengan cara berjalan");
    }

    public String getNama(){
        return nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public void setUang(int uang){
        this.uang = uang;
    }

    public int getUang(){
        return uang;
    }
}

