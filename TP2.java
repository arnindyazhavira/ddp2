import java.util.*;

public class TP2 {
    public static void main(String args[]) {
        System.out.println("Selamat datang di DDP2");
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama Fakultas apa yang anda ingin ketahui?: "); 
        String nama = sc.nextLine().toLowerCase(); 
        switch (nama) {
            case "Fasilkom":
                System.out.println("Fakultas Ilmu Komputer");
                break;
            case "FMIPA":
                System.out.println("Fakultas Matematika dan Ilmu Pengetahuan Alam");
                break;
            case "FIK":
                System.out.println("Fakultas Ilmu Komputer");
                System.out.println("Fakultas Ilmu Keperawatan");
                break;
            case "FK":
                System.out.println("Fakultas Kedokteran");
                break;
            case "FKG":
                System.out.println("Fakultas Kedokteran Gigi");
                break;
            case "FT":
                System.out.println("Fakultas Teknik");
                break;
            case "FKM":
                System.out.println("Fakultas Kesehatan Masyarakat");
                break;
            case "FF":
                System.out.println("Fakultas Farmasi");
                break;
            case "FPsi":
                System.out.println("Fakultas Psikologi");
                break;
            case "FISIP":
                System.out.println("Fakultas Ilmu Sosial dan Ilmu Politik");
                break;
            case "FIA":
                System.out.println("Fakultas Ilmu Administrasi");
                break;
            case "FEB":
                System.out.println("Fakultas Ekonomi & Bisnis");
                break;
            case "FIB":
                System.out.println("Fakultas Ilmu Budaya");
                break;
            default:
            System.out.println("Fakultas tidak ditemukan");
        }
    }
}

