import org.junit.Test;
import static org.junit.Assert.*;

public class DosenTest {
    @Test
    public void testDosen(){
        MataKuliah ddp2 = new MataKuliah("DDP2", "CSGE601021");
        Dosen pakade = new Dosen("Ade Azurat", "189271392");

        pakade.assignMatkul(ddp2);
        ddp2.assignDosen(pakade);

        assertEquals(pakade.getNama(), "Ade Azurat");
    }
}
