public class BurungHantu extends Hewan {

    public BurungHantu(String nama, String spesies){
        super(nama, spesies);
    }


    @Override
    public String bernafas(){
        return(getNama() + " bernafas dengan paru-paru");
    }

    @Override
    public String bersuara(){
        return("Hooooh hoooooooh.(Halo, saya " + getNama() + ". Saya adalah burung hantu).");
    }

    @Override
    public String bergerak(){
        return(getNama() + " bergerak dengan cara terbang");
    }

    public String toString(){
        return bersuara();
    }
}
