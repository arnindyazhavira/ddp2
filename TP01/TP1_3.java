import java.util.Scanner;

public class TP1_3 {
    public static void main(String[] args) {
        final int TAHUN_SEKARANG = 2020; // 'final' berarti tidak bisa diubah.
        double hargaBarang = 200000; // 'double' berarti floating point number
        Scanner inputTake = new Scanner(System.in);
        
        boolean tipeKartu;
        int percentage;
        int tahun;

        System.out.println("Selamat datang di bank Dedepedua!");
        System.out.print("Apa tipe kartu anda?: ");

        tipeKartu = inputTake.nextLong();
        
        System.out.print("Sejak tahun berapa anda menjadi member?: ");
        tahun = inputTake.nextLine();

        // Cek tipe kartu
        if (tipeKartu == "gold") {
            percentage = 25;
        } else if (tipeKartu == "silver") {
            percentage = 15;
        } else if (tipeKartu == "bronze") {
            percentage = 5;
        } else {
            percentage = 0;
        }

        // Cek tahun
        if (TAHUN_SEKARANG - tahun >= 3) {
            percentage += 5;
        }

        // Logika diskon barang
        // TODO: Implementasikan diskon dari persentase

        // Cetak Hasil
        System.out.println("Dapat diskon sebanyak " + percentage + " persen.");
        System.out.println("Harga barang menjadi Rp" + hargaBarang);

    }
}
