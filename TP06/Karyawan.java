public class Karyawan {
    private String nama;
    private int umur;
    private int lamaBekerja;
    private double gaji;

    //TODO
    public Karyawan(String nama, int umur, int lamaBekerja, double gaji) {
        this.nama = nama;
        this.umur = umur;
        this.lamaBekerja = lamaBekerja;
        this.gaji = gaji;
    }
    public Karyawan(){
    }

    //TODO
    public double getGaji(){
        return this.gaji;
    }
    

    public void setGaji(double gaji) {
        this.gaji = gaji;
    }

    public String getNama(){
        return this.nama;
    }

    public int getUmur(){
        return this.umur;
    }

    public int getLamaBekerja(){
        return this.lamaBekerja;
    }

    //Buatlah method setter getter yang diperlukan

}
