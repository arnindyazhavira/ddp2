public class RationalMatrix {
    @Override
    protected Rational multiply(Rational rat1, Rational rat2) {
      return rat1.multiply(rat2);
    }
    
    @Override
    protected Rational zero() {
      return new Rational(0,1);
    }

    @Override
    protected Rational add(Rational rat1, Rational rat2) {
      return rat1.add(rat2);
    }
}
