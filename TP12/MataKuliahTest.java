import org.junit.Test;
import static org.junit.Assert.*;


public class MataKuliahTest {
    @Test
    public void testKuliah01(){
        Mahasiswa mhs1 = new Mahasiswa("Burhan", "210283371");
        Mahasiswa mhs2 = new Mahasiswa("Bubur", "2102833571");
        MataKuliah kul1 = new MataKuliah("DDP2", "1906308293");

        kul1.tambahMhs(mhs1);
        kul1.tambahMhs(mhs2);
        kul1.dropMhs(mhs2);

        ArrayList<Mahasiswa> daftarMhs = kul1.getDaftarMhs();
        assertEquals(kul1.getNama(), "DDP2");
        assertEquals(kul1.getKode(), "1906308293");
        assertTrue(daftarMhs.contains(mhs1));
        assertFalse(daftarMhs.contains(mhs2));
    }

}
