import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        Kardus<Pakaian> pakaian = new Kardus<Pakaian>();
		Kardus<Barang> campuran = new Kardus<Barang>();
        Kardus<Elektronik> elek = new Kardus<Elektronik>();


        System.out.println("Selamat datang di layanan donasi Desa Dedepe Dua!");
		System.out.println("Layanan disponsori oleh Kedai VoidMain");
        System.out.println("Ada berapa jumlah barang untuk elektronik, pakaian, dan campuran?:");
        int a = inp.nextInt(); // elektronik
        int b = inp.nextInt(); // pakaian
        int c = inp.nextInt(); // campuran
        
        System.out.println("Silahkan masukan keterangan barang elektronik. Dengan format (jenis kondisi)");
        for (int i = 0; i<a; i++){
            String jenis = inp.next().toLowerCase();
            String kondisi = inp.next().toLowerCase();
            Elektronik elektronik = new Elektronik(jenis, kondisi);
            elek.tambahBarang(elektronik);
        }

        System.out.println("Silahkan masukan keterangan pakaian. Dengan format (ukuran warna)");
        for (int i = 0; i<b; i++){
            char ukuran = inp.next().toLowerCase().charAt(0);
            String warna = inp.next().toLowerCase();
            Pakaian pakaian2 = new Pakaian(ukuran, warna);
            pakaian.tambahBarang(pakaian2);
        }

        System.out.println("Silahkan masukan keterangan barang campuran. Dengan format (ELEKTRONIK jenis kondisi) atau (PAKAIAN ukuran warna) untuk pakaian");
        for (int i = 0; i<c; i++){
            String tipe = inp.nextLine().toLowerCase();
            if(tipe.equals("ELEKTRONIK")){
                String jenis = inp.nextLine().toLowerCase();
                String kondisi = inp.nextLine().toLowerCase();
                Elektronik elektronik = new Elektronik(jenis, kondisi);
                campuran.tambahBarang(elektronik);
            } else if(tipe.equals("PAKAIAN")){
                char ukuran = inp.next().toLowerCase().charAt(0);
                String warna = inp.nextLine().toLowerCase();
                Pakaian pakaian2 = new Pakaian(ukuran, warna);
                campuran.tambahBarang(pakaian2);
            }
        }

        
        System.out.println("-----------------------------------");
		System.out.println("Terima kasih atas donasi anda.");
		System.out.printf("Donasi anda sebesar %.2f DDD\n", (pakaian.getTotalValue()+elek.getTotalValue()+campuran.getTotalValue()));
		System.out.print("Rekap donasi untuk setiap kardus :\n---------------------------------\n");
		System.out.println(elek.rekap());
		System.out.println(pakaian.rekap());
		System.out.println(campuran.rekap());

		System.out.println("-----------------------------------");
    }
}
