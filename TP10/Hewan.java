public abstract class Hewan implements Makhluk{
    private String nama;
    private String spesies;

    public Hewan(String nama, String spesies){
        this.nama = nama;
        this.spesies = spesies;
    }

    public abstract String bersuara();


    public String getNama(){
        return nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getSpesies(){
        return spesies;
    }

    public void setSpesies(String spesies){
        this.spesies = spesies;
    }


}
