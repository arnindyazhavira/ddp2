public class Pakaian extends Barang{
    private char ukuran;
    private String warna;

    public Pakaian(char ukuran, String warna){
        this.ukuran = ukuran;
        this.warna = warna;
    }

    public char getUkuran(){
        return ukuran;
    }

    public void setUkuran(char ukuran) {
        this.ukuran = ukuran;
    }

    public String getWarna(){
        return warna;
    }
    
    public void setWarna(String warna) {
        this.warna = warna;
    }

    

    @Override
    public double getValue() {
        double value = 0.0;
        if (ukuran == 'l'){
            value = 40.0;
        } else if(ukuran == 'm'){
            value = 35.0;
        } else if(ukuran == 's'){
            value = 30.0;
        }
        return value;
    }

    //public String toString(){}
    
}
