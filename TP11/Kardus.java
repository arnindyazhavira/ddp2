import java.util.*;

public class Kardus<T extends Barang>{
    private List<T> listBarang = new ArrayList<>();


    public String rekap(){
        String tipe = "";
        int pakaian = 0;
        int elek= 0;

        for(T i : listBarang){
            try{
                Elektronik elektronik = (Elektronik) i;
                elek++;
            }
            catch(Exception exception){ 
                pakaian++;
            }
        }
        if(pakaian == 0){
            tipe = "Elektronik";
        } else if (elek == 0){
            tipe = "Pakaian";
        } else{
            tipe = "Campuran";
        }
        return("Kardus " + tipe + ": terdapat " + elek + "barang elektronik dan " + pakaian + " pakaian.");
    }

    public double getTotalValue(){
        double totalHarga = 0.0;
        for (T i : listBarang){
            totalHarga += i.getValue();
        }
        return totalHarga;
    }

    public void tambahBarang(T i){
        listBarang.add(i);
    }
}
