import java.io.*;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Dosen extends Manusia{
    private BufferedReader br;
    private FileReader fr;
    private String filename;
    private String nip;
    private FileWriter output;
    private ArrayList<String[]> temp = new ArrayList<String[]>();
    private ArrayList<MataKuliah> daftarMatkul = new ArrayList<>();
    private ArrayList<Dosen> daftarDosen = new ArrayList<>();

    public Dosen(String nama, String nip){
        super(nama);
        this.nip = nip;
    }

    public void assignMatkul(MataKuliah matkul){
        this.daftarMatkul.add(matkul);
    }

    public String getMatkul(){
        String nama;
        for(Dosen i: daftarDosen){
            if (i != null){
                nama += String.format("%s %n", i.getNama());
            }
        }
        return nama;
    }

    public String getNip(){
        return nip;
    }

    @Override
    public ArrayList<MataKuliah> getDaftarMatkul() {
        return daftarMatkul;
    }

    public ArrayLict<Dosen> getDaftarDosen(){
        return daftarDosen;
    }

    public ReadFile(String filename) throws IOException {
        try {
            this.filename = filename;
            fr = new FileReader(this.filename);
            br = new BufferedReader(fr);
            String line;

        while((line = br.readLine()) != null){
            temp.add(line.split(","));
        }
        return temp;
        
        }
        catch(FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
        br.close();
        fr.close();
    }
    

    public WriteFile(String fileName, String header) throws IOException {
        try{
            this.fileName = fileName;
            output = new FileWriter(this.fileName);
            output.write(header);
        }
        catch(FileNotFoundException e){
            System.out.println("File not found");
            e.printStackTrace();
        }
    }
}
