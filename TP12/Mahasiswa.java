import java.util.ArrayList;

public class Mahasiswa extends Manusia{
    private String nama;
    private String npm;
    private ArrayList<MataKuliah> daftarMatkul = new ArrayList<>();

    public Mahasiswa(String nama, String npm){
        this.nama = nama;
        this.npm = npm;
    }


    public String getNama(){
        return nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getNPM(){
        return npm;
    }

    public void setNPM(String npm){
        this.npm = npm;
    }

    public void tambahMatKul(ArrayList<MataKuliah> matkul){
        for (MataKuliah i: matkul){
            if (i == null){
                this.daftarMatkul.add(i);
            }
        }
    }

    public void dropMatKul(MataKuliah matkul){
        for (MataKuliah i: matkul){
            if (i.equals(matkul)){
                this.daftarMatkul.pop(i);
            }
        }
    }

    @Override
    public ArrayList<MataKuliah> getDaftarMatkul() {
        return daftarMatkul;
    }

    public void cetakMatkul(ArrayList<MataKuliah> matkul){
        for (int i = 0; i<matkul.size(); i++){
            if (matkul.get(i).getKode().startsWith("uige")){
                System.out.println(matkul.get(i).getNama() + " - Mata Kuliah Wajib Universitas");
            } else if (matkul.get(i).getKode().startsWith("uist")) {
                System.out.println(matkul.get(i).getNama() + " - Mata Kuliah Wajib Rumpun Sains dan Teknologi");
            } else if (matkul.get(i).getKode().startsWith("csge")) {
                System.out.println(matkul.get(i).getNama() + " - Mata Kuliah Wajib Fakultas");
            } else if (matkul.get(i).getKode().startsWith("cscm")) {
                System.out.println(matkul.get(i).getNama() + " - Mata Kuliah Wajib Program Studi Ilmu Komputer");
            } else if (matkul.get(i).getKode().startsWith("csim")) {
                System.out.println(matkul.get(i).getNama() + " - Mata Kuliah Wajib Program Studi Sistem Informasi");
            } else if (matkul.get(i).getKode().startsWith("csce")) {
                System.out.println(matkul.get(i).getNama() + " - Mata Kuliah Peminatan Program Studi Ilmu Komputer");
            } else if (matkul.get(i).getKode().startsWith("csie")) {
                System.out.println(matkul.get(i).getNama() + " - Mata Kuliah Peminatan Program Studi Sistem Informasi");
            }
        }
        
    }
    public void setDaftarMatkul(ArrayList<MataKuliah> daftarMatkul) {
        this.daftarMatkul = daftarMatkul;
    }
}


// UIGE = Mata Kuliah Wajib Universitas
// UIST = Mata Kuliah Wajib Rumpun Sains dan Teknologi
// CSGE = Mata Kuliah Wajib Fakultas
// CSCM = Mata Kuliah Wajib Program Studi Ilmu Komputer
// CSIM = Mata Kuliah Wajib Program Studi Sistem Informasi
// CSCE = Mata Kuliah Peminatan Program Studi Ilmu Komputer
// CSIE = Mata Kuliah Peminatan Program Studi Sistem Informasi

