public class Pegawai extends Manusia{
    private String levelKeahlian;

    public Pegawai(String nama, int uang, String levelKeahlian){
        super(nama, uang);
        this.levelKeahlian = levelKeahlian;
    }

    public String bekerja(){
        return(getNama() + " bekerja di kedai VoidMain");
    }

    public void setLevelKeahlian(String levelKeahlian){
        this.levelKeahlian = levelKeahlian;
    }

    public String getLevelKeahlian(){
        return levelKeahlian;
    }

    @Override
    public String bicara(){
        return("Halo, saya " + getNama() + ". Uang saya adalah " + getUang() + ", dan level keahlian saya adalah " + getLevelKeahlian() + ".");
    }

    public String toString(){
        return bicara();
    }
}
