import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        Scanner scan2 = new Scanner(System.in);
        int jumlKaryawan = scan.nextInt();
        ArrayList<Karyawan> listKaryawan = new ArrayList<Karyawan>();
        for (int i=0;i<jumlKaryawan; i++){
            String inputKaryawan = scan2.nextLine();
            String delimeter = " ";
            String[] tempArray = inputKaryawan.split(delimeter);
            String nama = tempArray[0];
            int umur = Integer.parseInt(tempArray[1]);
            int lamaKerja = Integer.parseInt(tempArray[2]);
            Karyawan karyawan1 = new Karyawan(nama,umur,lamaKerja,100.0);
            listKaryawan.add(karyawan1);
        }
        for (Karyawan karyawan:listKaryawan){
            int lamaKerja1 = karyawan.getLamaBekerja();
            for (int i=1; i<=lamaKerja1; i++){
                if (i%3 == 0){
                    double kenaikanGaji = karyawan.getGaji()*(0.05);
                    //System.out.println(karyawan.getNama());
                    //System.out.printf("ini kenaikan gaji %.2f\n",kenaikanGaji);
                    double currentGaji = kenaikanGaji + karyawan.getGaji();
                    //System.out.printf("ini current gaji %.2f\n",currentGaji);
                    karyawan.setGaji(currentGaji);
                }
            }
            if (karyawan.getUmur()>40){
                double currentGaji = 10.0+karyawan.getGaji();
                karyawan.setGaji(currentGaji);
            }

        }
        double average = rerataGaji(listKaryawan);
        String tertinggi = gajiTertinggi(listKaryawan);
        String terendah = gajiTerendah(listKaryawan);

        System.out.printf("Rata-rata gaji karyawan adalah %.2f\n", average);
        System.out.println("Karyawan dengan gaji tertinggi adalah " + tertinggi);
        System.out.println("Karyawan dengan gaji terendah adalah " + terendah);
    }

    //TODO
    public static double rerataGaji(ArrayList<Karyawan> listKaryawan) {
        double totalGaji = 0.0;
        for (Karyawan karyawan:listKaryawan){
            totalGaji+=karyawan.getGaji();
        }
        return totalGaji/listKaryawan.size();
    }
    
    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTerendah(ArrayList<Karyawan> listKaryawan) {
        Karyawan gajikaryawan = new Karyawan();
        double temp = 0.0;
        for (Karyawan karyawan:listKaryawan){
            if (karyawan.getGaji()<temp){
                temp = karyawan.getGaji();
                gajikaryawan = karyawan;
            } else if (temp == 0.0){
                temp = karyawan.getGaji();
                gajikaryawan = karyawan;
            }
        }
        return gajikaryawan.getNama();
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTertinggi(ArrayList<Karyawan> listKaryawan) {
        Karyawan gajikaryawan = new Karyawan();
        double temp = 0.0;
        for (Karyawan karyawan:listKaryawan){
            if (karyawan.getGaji()>temp){
                temp = karyawan.getGaji();
                gajikaryawan = karyawan;
            }
        }
        return gajikaryawan.getNama();
    }

}
