public class Elektronik extends Barang{
    private String jenis;
    private String kondisi;

    public Elektronik(String jenis, String kondisi){
        this.jenis = jenis;
        this.kondisi = kondisi;
    }

    public String getJenis(){
        return jenis;
    }

    public void setJenis(String jenis){
        this.jenis = jenis;
    }

    public String getKondisi(){
        return kondisi;
    }

    public void setKondisi(String kondisi){
        this.kondisi = kondisi;
    }

    //public String toString(){}

    public double getPrice(){
        double price = 0.0;
        if (jenis.equals("hp")){
            price = 200.0;
        } else if (jenis.equals("laptop")){
            price = 500.0;
        } else if (jenis.equals("modem")){
            price = 100.0;
        }

        return price;
    }

    @Override
    public double getValue() {
        double value = 0.0;
        if(kondisi.equals("buruk")){
            value = getPrice() * 0.25;
        } else if(kondisi.equals("baik")){
            value = getPrice();
        } else if(kondisi.equals("menengah")){
            value = getPrice() * 0.8;
        } else if(kondisi.equals("baru")){
            value = getPrice() * 1.25;
        }

        return value;
    }


}
